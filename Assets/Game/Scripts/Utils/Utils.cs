namespace Game {
    using System.Collections.Generic;
    using System.IO;

    public static partial class Utils {
        public static List<string> GetFiles(string path, string ext = null, bool recursive = true) {
            List<string> ret = new List<string>();

            string[] dirs = Directory.GetDirectories(path);
            string[] files = Directory.GetFiles(path);

            foreach (string file in files) {
                if (file.EndsWith(".meta")) continue;
                if (!string.IsNullOrEmpty(ext) && Path.GetExtension(file) != ext) continue;
                ret.Add(file.Replace("\\", "/"));
            }

            if (recursive) {
                foreach (string dir in dirs) ret.AddRange(GetFiles(dir, ext));
            }

            return ret;
        }
    }
}