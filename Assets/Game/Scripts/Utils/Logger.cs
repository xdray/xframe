namespace Game {
    using System.Collections;

    using O = UnityEngine.Debug;
    
    /// <summary>
    ///     Wrapper for UnityEngine.Debug.LogXXX
    /// </summary>
    public static class Logger {

        /// <summary>
        ///     Log message level.
        /// </summary>
        public enum Level {
            Exception,
            Error,
            Warning,
            Assert,
            Info,
            Debug,
        }

        /// <summary>
        ///     Max allow message level.
        /// </summary>
        public static Level maxAllow = Level.Info;

        /// <summary>
        ///     Initialization.
        /// </summary>
        public static IEnumerator OnInit() {
            // TODO
            // maxAllow = (Level)Configure.Get<int>("Game", "MaxLogLevel");
            //
            //Info("--> Logger.Initialize set log level : {0}", maxAllow.ToString());
            yield return null;
        }

        /// <summary>
        ///     Log exception.
        /// </summary>
        /// <param name="e">Exception content.</param>
        public static void Exception(System.Exception e) {
            O.LogException(e);
        }

        /// <summary>
        ///     Log error message
        /// </summary>
        /// <param name="fmt">Message format</param>
        /// <param name="args">Parameters</param>
        public static void Error(string fmt, params object[] args) {
            O.LogErrorFormat(fmt, args);
        }

        /// <summary>
        ///     Log warning message
        /// </summary>
        /// <param name="fmt">Message format</param>
        /// <param name="args">Parameters</param>
        public static void Warn(string fmt, params object[] args) {
            if (maxAllow >= Level.Warning) O.LogWarningFormat(fmt, args);
        }

        /// <summary>
        ///     Assert
        /// </summary>
        /// <param name="evl">Condition</param>
        /// <param name="fmt">Message format</param>
        /// <param name="args">Parameters</param>
        public static void Assert(bool evl, string fmt, params object[] args) {
            if (maxAllow >= Level.Assert) O.AssertFormat(evl, fmt, args);
        }

        /// <summary>
        ///     Log important information.
        /// </summary>
        /// <param name="fmt">Message format</param>
        /// <param name="args">Parameters</param>
        public static void Info(string fmt, params object[] args) {
            if (maxAllow >= Level.Info) O.LogFormat(fmt, args);
        }

        /// <summary>
        ///     Log debug message.
        /// </summary>
        /// <param name="fmt">Message format</param>
        /// <param name="args">Parameters</param>
        public static void Debug(string fmt, params object[] args) {
            if (maxAllow >= Level.Debug) O.LogFormat(fmt, args);
        }
    }
}
