﻿namespace Game {
    using UnityEngine;
    using UnityEngine.UI;

    public class Entry : MonoBehaviour {
        [SerializeField] Slider Progress;
        [SerializeField] Text   Message;

        private void FixedUpdate() {
            Progress.value = Updater.Progress;
        }
    }
}
