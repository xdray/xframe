namespace Game {
    using UnityEngine;
    using UnityEngine.UI;

    public class Loading : MonoBehaviour {
        [SerializeField] Slider Progress;

        private void FixedUpdate() {
            Progress.value = MapLoader.Progress;
        }
    }
}
