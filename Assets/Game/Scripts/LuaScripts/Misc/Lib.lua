-- ========================================================
-- @File	: Lib.lua
-- @Brief	: 通用全局工具类库
-- @Author	: Dray
-- @Date	: 2018-03-26
-- ========================================================

function Inherit(tbBase)
    local tb = { super = tbBase }
    setmetatable(tb, { __index = function(tb, key) return tb.super[key] end })
    return tb
end

function Eval(s)
	return assert(load('return ' .. (s or '')))();
end

function DoFile(sPath)
    local sCode = G.LuaUtils.LoadLua(sPath)
    if not sCode then error("Missing lua file: " .. sPath) end
    return assert(load(sCode, sPath))()
end

function LoadCsvFile(sPath, nHeader)
	local sContent = G.LuaUtils.LoadText(sPath)
	if not sContent or #sContent <= 0 then return {} end

	nHeader		= nHeader or 1
	sContent	= string.gsub(sContent, "\r\n", "\n")
	sContent	= string.gsub(sContent, "\r", "\n")

	local tbLine	= Split(sContent, "\n")
	local nCount	= #tbLine

	if nCount <= nHeader then return {} end
	
	local tbKey		= Split(tbLine[nHeader], '\t')
	local tbData	= {}

	for i = nHeader + 1, nCount do
		local sLine	= tbLine[i]
		if (string.len(string.gsub(sLine, " |\t", ""))) > 0 then
			local tbVal	= Split(sLine, "\t")
			local tbAtt = {}

			for nCol, sVal in ipairs(tbVal) do
				if tbKey[nCol] then 
					if sVal == "" then
						tbAtt[tbKey[nCol]] = nil 
					else
						tbAtt[tbKey[nCol]] = sVal 
					end
				end
			end

			if CountTB(tbAtt) > 0 then table.insert(tbData, tbAtt) end
		end
	end
	
	return tbData
end

-- 加载Prefab
function Prefab(sPath)
	return G.LuaUtils.LoadPrefab(sPath .. '.prefab');
end
