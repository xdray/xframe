-- ========================================================
-- @File	: UI.lua
-- @Brief	: UI管理器
-- @Author	: Dray
-- @Date	: 2018-03-27
-- ========================================================

-------------------------------------------------------------------------------
-- 全局辅助函数

-- 设置显示隐藏
function Show(data, bShow)
    if bShow == nil then bShow = true end

    local pDoShow = function(obj) if v.gameObject.activeSelf ~= bShow then v.gameObject:SetActive(bShow) end end

    if type(data) == 'table' then
        for _, o in pairs(data) do pDoShow(o) end
    else
        pDoShow(data)
    end
end

-- 反转显示隐藏
function ShowOrHide(data)
    local pDoShowOrHide = function(obj) obj.gameObject:SetActive(not obj.gameObject.activeSelf) end

    if type(data) == 'table' then
        for _, o in pairs(data) do pDoShowOrHide(o) end
    else
        pDoShowOrHide(data)
    end
end

-------------------------------------------------------------------------------
-- 界面管理模块

UI = UI or { tbClass = {}, tbStack = {}, tbConfig = {} }

-- 界面逻辑基类
UI.tbBase = require('UI/UI_Default.lua')

-------------------------------------------------------------------------------
-- 外部接口

-- 注册界面
function UI.Register(sClass)
    local t = Inherit(UI.tbBase)

    t.sClass    = sClass
    t.pRoot     = nil

    UI.tbClass[sClass] = t
    return t
end

-- 打开界面
function UI.Open(sClass, ...)
    local tbLogic = UI.__Load(sClass)
    if not tbLogic then return end

    tbLogic:DoOpen(false, '', ...)

    local tbCfg = UI.tbConfig[sClass]
    if tbCfg and tbCfg.bStack then UI.Push(sClass) end

    return tbLogic
end

-- 关闭单个界面
function UI.Close(v, pCallback)
    local tbLogic = nil

    local vtype = type(v)
    if vtype == 'string' then
        tbLogic = UI.tbClass[v]
    elseif vtype == 'table' then
        tbLogic = v
    else
        error('UI.Close #1 must be a string value or UI class')
    end

    local tbInfo = tbLogic.__CurrStack()
    if (not tbInfo or not tbInfo.Children) and tbLogic ~= UI.TopClass() then
        error('UI.Close close stack top ui or child ui only')
    end

    local pFunc = function()
        local tbCfg = UI.tbConfig[tbLogic.sClass]
        if tbCfg and tbCfg.bStack then UI.Pop() end

        if not pCallback then pCallback() end
    end

    tbLogic:DoClose(pFunc)
end

-- 关闭所有界面
function UI.CloseAll()
end

-- 界面回退
function UI.Back()
    local tbLogic = UI.TopClass()
    if not tbLogic then return end

    local bReturn = tbLogic:OnReturn()
    if not bReturn then return end

    UI.Close(sClass)

    tbLogic = UI.TopClass()
    if not tbLogic then return end
    tbLogic:__Restore()
end

-- 判断界面是否打开
function UI.IsOpened(sClass)
    local tbLogic = UI.GetClass(sClass)
    if not tbLogic or not tbLogic:__CurrStack() then return false end
    return true
end

-- 调用界面函数
function UI.Call(sClass, sFunc, ...)
end

-- 获取界面逻辑
function UI.GetClass(sClass)
    return UI.tbClass[sClass]
end

-- 栈顶界面
function UI.TopClass()
    local nIndex = #UI.tbStack
    if nIndex == 0 then return nil end
    return UI.GetClass(UI.tbStack[nIndex])
end

-- 栈Push操作
function UI.Push(tbInfo)
    table.insert(UI.tbStack, tbInfo)
end

-- 栈Pop操作
function UI.Pop()
    local nIndex = #UI.tbStack
    if nIndex == 0 then return end
    UI.tbStack[nIndex] = nil
end

-------------------------------------------------------------------------------
-- 内部接口

-- 加载界面
function UI.__Load(sClass)
    if UI.tbClass[sClass] then return UI.tbClass[sClass] end

    local obj = Prefab('UI/' .. sClass)
    if not obj then
        error('UI.Open ' .. sClass .. 'failed. Missing prefab.')
    end

    DoFile('UI/' .. sClass .. '.lua')

    local tbLogic = UI.tbClass[sClass]
    local pRoot = U.GameObject.Instantiate(obj, G.CanvasCamera.Root)

    tbLogic.pRoot = pRoot
    UI.__SetCamera(tbLogic)
    tbLogic:OnInit()

    return tbLogic
end

-- 设置界面Canvas模式
function UI.__SetCamera(tbLogic)
    if not tbLogic:IsCustomRenderMode() then
        if not tbLogic.pRoot then return end

        local pCanvas = tbLogic.pRoot:GetComponent('Canvas')
        pCanvas.renderMode = U.RenderMode.ScreenSpaceCamera
       
        -- local nLayer = tbLogic.pRoot.gameObject.layer
		-- if nLayer == G.Layers.Background then
		-- 	pCanvas.worldCamera = G.BackgoundCamera.Camera
		-- else
		-- 	pCanvas.worldCamera = G.CanvasCamera.Camera
        -- end

        pCanvas.worldCamera = G.CanvasCamera.Camera

        -- 使用配置中的层级
        -- local tbCfg = UI.tbConfig[tbLogic.sClass]
        -- if tbCfg then pCanvas.sortingOrder = tbCfg.nLayer end
    end
end

-- 读取Ui配置
function UI.__LoadUiConfig()
    local tbConfig = LoadCsvFile('Ui.txt', 2)
    for _, v in pairs(tbConfig) do
        local tbc = {}
        tbc.nLayer = tonumber(v.Layer)
        tbc.bStack = tonumber(v.Stack) > 0
        UI.tbConfig[v.Name] = tbc
    end
end
