-- ========================================================
-- @File	: UI_Default.lua
-- @Brief	: UI界面基类
-- @Author	: Dray
-- @Date	: 2018-03-27
-- ========================================================

local tbClass = {}

tbClass.sClass      = ''
tbClass.pRoot       = nil

-- { {bOpend: bool, sParent: str, vParams: table, Children: table}, ... }
tbClass.tbStack     = {}

-------------------------------------------------------------------------------
-- 回调继承

function tbClass:OnInit()
end

function tbClass:OnOpen()
end

function tbClass:OnClose()
end

function tbClass:OnRelease()
end

-- 返回事件
-- true         返回
-- not ture     不处理
function tbClass:OnReturn()
    return true
end

-- 是否自定义Camera
function tbClass:IsCustomRenderMode()
    return false
end


-------------------------------------------------------------------------------
-- 外部接口

function tbClass:OpenAsChild(sClass, ...)
    local tbLogic = UI.__Load(sClass)
    if not tbLogic then return end

    tbLogic:DoOpen(true, self.sClass, ...)

    return tbLogic
end

-- 获取父界面
function tbClass:Parent()
    local tbInfo = self:__CurrStack()
    if not tbInfo or not tbInfo.Children then return nil end
    return UI.GetClass(tbInfo.sParent)
end

-- 显示隐藏界面
function tbClass:Show(bShow)
    self.pRoot.gameObject:SetActive(bShow ~= false)
end

function tbClass:GetComponent(sType, sPath, pFrom)
	local trans = pFrom or self.pRoot.transform
	if sPath and #sPath > 0 then trans = trans:Find(sPath) end
	if not trans then return nil end
	return trans:GetComponent(sType)
end

function tbClass:Find(sPath, pFrom)
	if not pFrom then
		return self.pRoot.transform:Find(sPath);
	else
		return pFrom.transform:Find(sPath);
	end
end

function tbClass:OnClick(sPath, pFun, pFrom)
	local btn = self:GetComponent("Button", sPath, pFrom); btn.onClick:AddListener(pFun)
end

function tbClass:OnClickAndClear(sPath, pFun, pFrom)
	local btn = self:GetComponent("Button", sPath, pFrom)
	btn.onClick:RemoveAllListeners(); btn.onClick:AddListener(pFun)
end

-------------------------------------------------------------------------------
-- 内部事件

function tbClass:DoOpen(bChild, sParent, ...)
    if not self.pRoot then return end

    local tbInfo = {}
    tbInfo.bOpend   = true
    tbInfo.sParent  = sParent
    tbInfo.vParams  = {...}

    tbInfo.Children = nil
    if bChild then tbInfo.Children = {} end

    table.insert(self.tbStack, tbInfo)

    self:Show()
    self:OnOpen(...)
end

function tbClass:DoClose(pCallback)
    local tbInfo = self:__CurrStack()
    if not tbInfo then return end

    -- 关闭所有子窗口
    if tbInfo.Children then
        for name, _ in pairs(tbInfo.Children) do UI.Close(name) end
    end

    self:Show(false)
    self:OnClose()

    self.tbStack[#self.tbStack] = nil

    if pCallback then pCallback() end
end

function tbClass:DoRelease()
    local tbInfo = self:__CurrStack()
    if not tbInfo then return end

    if tbInfo.bOpend then UI.Close(self.sClass) end
    self:OnRelease()

    local pObj = self.pRoot
    for k, v in pairs(self) do
        if type(v) == 'userdata' then self[k] = nil end
    end
    U.Object.Destroy(pObj)
    pObj = nil
end

-------------------------------------------------------------------------------
-- 内部接口

function tbClass:__CurrStack()
    local nIndex = #self.tbStack
    if nIndex == 0 then return nil end
    return self.tbStack[nIndex]
end

-- 回复栈顶状态
function tbClass:__Restore()
    local tbInfo = self.__CurrStack()
    if not tbInfo then return end

    self:Show()
    self:Open(tbInfo.vParams)

    if tbInfo.Children then
        for _, name in ipairs(tbInfo.Children) do UI.GetClass(name):__Restore() end
    end
end

return tbClass
