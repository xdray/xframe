-- ========================================================
-- @File	: Entry.lua
-- @Brief	: Entry场景逻辑
-- @Author	: Dray
-- @Date	: 2018-03-26
-- ========================================================

local tbClass = Map.Register('Entry')

function tbClass:OnLoaded()
    self.super:OnLoaded(); G.Map.Load(1)
end
