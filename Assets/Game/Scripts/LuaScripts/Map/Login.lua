-- ========================================================
-- @File	: Login.lua
-- @Brief	: Login场景逻辑
-- @Author	: Dray
-- @Date	: 2018-03-26
-- ========================================================

local tbClass = Map.Register('Login')

function tbClass:OnLoaded()
    self.super:OnLoaded()

    UI.Open('UI_Login')
end
