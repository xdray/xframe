-- ========================================================
-- @File	: Map.lua
-- @Brief	: 场景管理器
-- @Author	: Dray
-- @Date	: 2018-03-26
-- ========================================================

Map = Map or { tbClass = {} }

local tbTmpl = {
	OnLoading	= function() end,
	OnLoaded	= function() end,
	OnLeave		= function() end,
}

function Map.OnLoading(tbInfo)
	Map.Get(tbInfo):OnLoading()
end

function Map.OnLoaded(tbInfo)
	Map.Get(tbInfo):OnLoaded()
end

function Map.OnLeave(tbInfo)
	Map.Get(tbInfo):OnLeave()
end

function Map.Register(sClass)
	local tb = Inherit(tbTmpl)
	Map.tbClass[sClass] = tb
	return tb
end

function Map.Get(tbInfo)
	if not tbInfo then return tbTmpl end

	if Map.tbClass[tbInfo.Type] then return Map.tbClass[tbInfo.Type] end

	xpcall(
		function()
			require('Map/' .. tbInfo.Type .. '.lua')
		end,
		function(err)
			print('Load Map logic failed', tbInfo.Type, err)
		end
	)

	return Map.tbClass[tbInfo.Type] or tbTmpl
end