﻿namespace Game {
	using System.Collections;

	using UnityEngine;

	public class CanvasCamera : MonoBehaviour {
		public static Camera Camera { get; private set; }

		public static Transform Root {
            get {
                if (root == null) root = new GameObject("UI");
                return root.transform;
            }
        }

		public static float ScreenAspectRatio {
            get {
                return Screen.width * 1.0f / Screen.height;
            }
        }

		public static IEnumerator OnInit() {
			GameObject proxy = null;
			yield return Assets.InstantiateAsync<GameObject>("Camera/Canvas.prefab", o => proxy = o);

			if (proxy == null) yield break;

			proxy.name = "CanvasCamera";
			Camera = proxy.GetComponent<Camera>();
			proxy.transform.position = Vector3.zero;

			DontDestroyOnLoad(proxy);
		}

		private static GameObject root = null;
	}
}