namespace Game {
    using System.Collections;

    using UnityEngine;

    public static class PlayerSetting {
        public static IEnumerator OnInit() {
            yield return null;
        }

        public static int UnpackVersion {
            get { return unpackVersion; }
            set {
                if (value != unpackVersion) {
                    unpackVersion = value; PlayerPrefs.SetInt("UnpackVersion", value);
                }
            }
        }

        private static int unpackVersion = 0;
    }
}