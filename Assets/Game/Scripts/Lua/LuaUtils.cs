namespace Game {
    using System.IO;

    using UnityEngine;

    public static class LuaUtils {
        public static string LoadLua(string path) {
#if UNITY_EDITOR
            var fullPath = Dir.LuaScriptsPath + path;
            if (!File.Exists(fullPath)) return null;
            return File.ReadAllText(fullPath);
#else
            var asset = Assets.Load<TextAsset>(path);
            return asset != null ? asset.text : null;
#endif
        }

        public static string LoadText(string path) {
            var asset = Assets.Load<TextAsset>(path);
            return asset != null ? asset.text : null;
        }

        public static GameObject LoadPrefab(string path) {
            return Assets.Load<GameObject>(path);
        }
    }
}