namespace Game {
    using System.Collections.Generic;
    using System.IO;

    public static partial class Utils {
        public static void WriteBundle2Local(string name, byte[] data) {
            string fullPath = Dir.Storage + name;
            if (!Directory.Exists(fullPath)) {
                Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
            }

            FileStream fs = new FileStream(fullPath, FileMode.OpenOrCreate);
            fs.Write(data, 0, data.Length);
            fs.Flush(); fs.Close();
        }
    }
}