namespace Game {
    using System.Collections;
    using System.Collections.Generic;

    using UnityEngine;

    using Newtonsoft.Json;

    public class PackageInfo {
        public static PackageInfo Ins;

        public static IEnumerator OnInit() {
            WWW req = new WWW(Dir.Streaming + "package.json"); yield return req;

            if (req.error == null) {
                Ins = JsonConvert.DeserializeObject<PackageInfo>(req.text);
            } else {
                Logger.Warn("PackageInfo load failed: {}. Using default.", req.error);
                Ins = new PackageInfo();
            }

            req.Dispose();
        }

        public string AppId = "com.kingsoft";

        public int Major = 0;

        public int Minor = 0;

        public int Build = 0;

        public bool Development = true;

        public bool EnableUpdate = true;

        public string UpdateUrl = "";

        public List<string> PackedBundles = new List<string>();

        [JsonIgnore]
        public int Version { get { return Major * 10000 + Minor * 100 + Build; } }

        [JsonIgnore]
        public string BundleVersion { get { return string.Format("{0}.{1}.{2}", Major, Minor, Build); } }
    }
}