namespace Game {
    using System.Collections.Generic;
    using System.IO;

    using UnityEngine;

    using UA = UnityEngine.Application;

    public static class Dir {
        public static string Storage {
            get { return UA.persistentDataPath + "/"; }
        }

        public static string Streaming {
            get {
                switch (UA.platform) {
                    case RuntimePlatform.Android: return "jar:file://" + UA.dataPath + "/!/assets/";
                    case RuntimePlatform.IPhonePlayer: return "file://" + UA.dataPath + "/Raw/";
                    default: return "file://" + UA.dataPath + "/StreamingAssets/";
                }
            }
        }

        public static string Platform {
            get {
                switch (UA.platform) {
                    case RuntimePlatform.Android: return "Android";
                    case RuntimePlatform.IPhonePlayer: return "IOS";
                    default: return "PC";
                }
            }
        }

        #region 更新路径
        public static string UpdateUrl {
            get {
                return baseUpdateUrl + Platform + "/";
            }
        }

        public static void SetUpdateUrl(string url) {
            baseUpdateUrl = url;
        }
        #endregion

        #region LUA路径
        public static readonly string LuaAbPath = "Assets/Game/AssetBundles/Lua/";
        public static readonly string LuaGenPath = "Assets/Game/Scripts/LuaObject/";
        public static readonly string LuaScriptsPath = "Assets/Game/Scripts/LuaScripts/";
        #endregion

        public static readonly string A2BPath       = "Assets/Game/AssetBundles/a2b.json";
        public static readonly string PackagePath   = "Assets/StreamingAssets/package.json";

        public static string RealPathInBundle<T>(string shortPath) {
            if (shortPath.StartsWith("Assets/")) return shortPath;

            var type = typeof(T);
            if (type == typeof(TextAsset)) {
                if (shortPath.EndsWith(".lua")) {
#if UNITY_EDITOR
                    return "Assets/Game/Scripts/LuaScript/" + shortPath;
#else
                    return LuaAbPath + shortPath + ".txt";
#endif
                } else {
                    return "Assets/Game/AssetBundles/Settings/" + shortPath;
                }   
            } else if (type == typeof(GameObject)) {
                return "Assets/Game/AssetBundles/Prefabs/" + shortPath;
            }

            return "Assets/Game/AssetBundles/" + shortPath;
        }

        private static string baseUpdateUrl = "";
    }
}