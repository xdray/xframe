﻿namespace Game {
    using System.Collections.Generic;

    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;

    public static class LuaSetting {

        [CSObjectWrapEditor.GenPath]
        public static string GenPath = "Assets/Game/Scripts/LuaObject/";

        [XLua.ReflectionUse]
        public static List<System.Type> defaultReflectionUse = new List<System.Type>() {
            typeof(System.ComponentModel.TypeConverter),
            typeof(System.ComponentModel.ArrayConverter),
            typeof(System.ComponentModel.BaseNumberConverter),
            typeof(System.ComponentModel.BooleanConverter),
            typeof(System.ComponentModel.ByteConverter),
            typeof(System.ComponentModel.CharConverter),
            typeof(System.ComponentModel.CollectionConverter),
            typeof(System.ComponentModel.ComponentConverter),
            typeof(System.ComponentModel.CultureInfoConverter),
            typeof(System.ComponentModel.DateTimeConverter),
            typeof(System.ComponentModel.DecimalConverter),
            typeof(System.ComponentModel.DoubleConverter),
            typeof(System.ComponentModel.EnumConverter),
            typeof(System.ComponentModel.ExpandableObjectConverter),
            typeof(System.ComponentModel.Int16Converter),
            typeof(System.ComponentModel.Int32Converter),
            typeof(System.ComponentModel.Int64Converter),
            typeof(System.ComponentModel.NullableConverter),
            typeof(System.ComponentModel.SByteConverter),
            typeof(System.ComponentModel.SingleConverter),
            typeof(System.ComponentModel.StringConverter),
            typeof(System.ComponentModel.TimeSpanConverter),
            typeof(System.ComponentModel.UInt16Converter),
            typeof(System.ComponentModel.UInt32Converter),
            typeof(System.ComponentModel.UInt64Converter),
        };

        [XLua.CSharpCallLua]
        public static List<System.Type> defaultLuaCallback = new List<System.Type>() {
            typeof(System.Action),
            typeof(System.Action<bool>),
            typeof(System.Action<int>),
            typeof(System.Action<float>),
            typeof(System.Action<string>),
            typeof(System.Action<MapInfo>),
            typeof(UnityAction<System.Object, System.Object, System.Object, System.Object>),
            typeof(UnityAction),
            typeof(UnityAction<bool>),
            typeof(UnityAction<int>),
            typeof(UnityAction<float>),
            typeof(UnityAction<string>),
            typeof(UnityAction<Vector2>),
            typeof(UnityAction<Vector3>),
            typeof(UnityAction<Vector4>),
            typeof(UnityAction<GameObject>),
            typeof(UnityAction<PointerEventData>),
        };

        [XLua.LuaCallCSharp]
        public static List<System.Type> defaultCSharpCallback = new List<System.Type>() {
            typeof(System.Action),
            typeof(System.Action<bool>),
            typeof(System.Action<int>),
            typeof(System.Action<float>),
            typeof(System.Action<string>),
            typeof(System.Action<Vector2>),
            typeof(System.Action<Vector3>),
            typeof(System.Action<Vector4>),
            typeof(System.Action<GameObject>),
            typeof(System.Action<PointerEventData>),


            typeof(List<object>),
            typeof(List<KeyValuePair<string, string>>),

            typeof(UnityEngine.Canvas),
            typeof(System.Object),
            typeof(UnityEngine.Object),
            typeof(UnityEngine.Vector2),
            typeof(UnityEngine.Vector3),
            typeof(UnityEngine.Vector4),
            typeof(UnityEngine.Quaternion),
            typeof(UnityEngine.Color),
            typeof(UnityEngine.Ray),
            typeof(UnityEngine.Time),
            typeof(UnityEngine.GameObject),
            typeof(UnityEngine.Component),
            typeof(UnityEngine.Behaviour),
            typeof(UnityEngine.Transform),
            typeof(UnityEngine.Resources),
            typeof(UnityEngine.TextAsset),
            typeof(UnityEngine.MonoBehaviour),
            typeof(UnityEngine.Renderer),
            typeof(UnityEngine.Debug),
            typeof(UnityEngine.Camera),
            typeof(UnityEngine.RenderMode),
            typeof(UnityEngine.RectTransformUtility),
            typeof(UnityEngine.RectTransform),
            typeof(UnityEngine.CameraClearFlags),
            typeof(UnityEngine.CameraType),
            typeof(UnityEngine.CanvasGroup),
            typeof(UnityEngine.CanvasRenderer),
            typeof(UnityEngine.Color32),
            typeof(UnityEngine.EventSystems.EventSystem),
            typeof(UnityEngine.FilterMode),
            typeof(UnityEngine.LayerMask),
            typeof(UnityEngine.Rect),
            typeof(UnityEngine.Renderer),
            typeof(UnityEngine.RenderTexture),
            typeof(UnityEngine.Screen),
            typeof(UnityEngine.ScreenOrientation),
            typeof(UnityEngine.Shader),
            typeof(UnityEngine.Sprite),
            typeof(UnityEngine.SystemInfo),
            typeof(UnityEngine.Texture),
            typeof(UnityEngine.Texture2D),

            typeof(UnityEngine.UI.Button),
            typeof(UnityEngine.UI.Image),
            typeof(UnityEngine.UI.RawImage),
            typeof(UnityEngine.UI.ScrollRect),
            typeof(UnityEngine.UI.Text),
            typeof(UnityEngine.UI.Slider),
            typeof(UnityEngine.UI.ToggleGroup),
            typeof(UnityEngine.UI.Toggle),
            typeof(UnityEngine.UI.RectMask2D),
            typeof(UnityEngine.UI.Scrollbar),
            typeof(UnityEngine.UI.Mask),
            typeof(UnityEngine.UI.LayoutUtility),
            typeof(UnityEngine.UI.LayoutElement),
            typeof(UnityEngine.UI.InputField),
            typeof(UnityEngine.UI.Dropdown),
            typeof(UnityEngine.UI.ContentSizeFitter),
            typeof(UnityEngine.UI.LayoutElement),
            typeof(UnityEngine.UI.CanvasScaler),
            typeof(UnityEngine.UI.CanvasUpdate),
            typeof(UnityEngine.UI.GridLayoutGroup),
            typeof(UnityEngine.UI.VerticalLayoutGroup),
            typeof(UnityEngine.UI.HorizontalLayoutGroup),
            typeof(UnityEngine.UI.Outline),
            typeof(UnityEngine.SystemInfo),
         
            typeof(Game.Downloader),
        };


        //黑名单
        [XLua.BlackList]
        public static List<List<string>> BlackList = new List<List<string>>()  {
            new List<string>(){"UnityEngine.WWW", "movie"},
#if UNITY_WEBGL
            new List<string>(){"UnityEngine.WWW", "threadPriority"},
#endif
            new List<string>(){"UnityEngine.Texture2D", "alphaIsTransparency"},
            new List<string>(){"UnityEngine.Texture", "imageContentsHash"},
            new List<string>(){"UnityEngine.Security", "GetChainOfTrustValue"},
            new List<string>(){"UnityEngine.CanvasRenderer", "onRequestRebuild"},
            new List<string>(){"UnityEngine.Light", "areaSize"},
            new List<string>(){"UnityEngine.AnimatorOverrideController", "PerformOverrideClipListCleanup"},
#if !UNITY_WEBPLAYER
            new List<string>(){"UnityEngine.Application", "ExternalEval"},
#endif
            new List<string>(){"UnityEngine.GameObject", "networkView"}, //4.6.2 not support
            new List<string>(){"UnityEngine.Component", "networkView"},  //4.6.2 not support
            new List<string>(){"System.IO.FileInfo", "GetAccessControl", "System.Security.AccessControl.AccessControlSections"},
            new List<string>(){"System.IO.FileInfo", "SetAccessControl", "System.Security.AccessControl.FileSecurity"},
            new List<string>(){"System.IO.DirectoryInfo", "GetAccessControl", "System.Security.AccessControl.AccessControlSections"},
            new List<string>(){"System.IO.DirectoryInfo", "SetAccessControl", "System.Security.AccessControl.DirectorySecurity"},
            new List<string>(){"System.IO.DirectoryInfo", "CreateSubdirectory", "System.String", "System.Security.AccessControl.DirectorySecurity"},
            new List<string>(){"System.IO.DirectoryInfo", "Create", "System.Security.AccessControl.DirectorySecurity"},
            new List<string>(){"UnityEngine.MonoBehaviour", "runInEditMode"},

            new List<string>(){"UnityEngine.UI.Text", "OnRebuildRequested"},
        };


    }
}

