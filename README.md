# xframe

xframe是UNITY快速开发框架。提供如下功能：

- 集成xLua
- UGUI管理器
- AssetBundle管理
- 打包工具 Packer

## 目录结构
```
-\
 |- Assets
    |- Game
        |- AssetBundles         存放需要打Bundle打文件
        |- Maps                 存放Level文件
        |- Scripts              所以代码
            |- Editor           Editor相关代码
            |- LuaScripts       Lua脚本
 |- Bundles
    |- PC                       PC平台Bundlee输出目录
    |- Android                  Android平台Bundlee输出目录
    |- IOS                      IOS平台Bundlee输出目录
 |- Doc                         文档
 |- Puhlish                     打包输出目录
 hfs.exe                        HTTP File Server
```

## Lua
xframe使用[xLua](https://github.com/Tencent/xLua)实现Lua调用。

xframe所有代码都在`Game`命名空间中实现。在Lua环境中，xframe定义两个全局变量`G`和`U`，分别表示`Game`与`UnityEngine`命名空间。

建议后续开发的C#代码放入`Game`命名空间。

## UGUI管理器
调试中。

## AssetBundle管理
由Packer定义、生成文件后缀为`.u`的Bundle文件到`Bundles/平台`中。打包时会拷贝对应平台的Bundle到`StreamingAsset`目录。

程序启动时，如果BundleVersion不一致，更新器`Updater`会下载需要下载的Bundle到`Application.persistentDataPath`目录。BundleVersion不一致存在两种情况，一是首次安装程序，二是更新覆盖安装程序。

程序启动时，更新器`Updater`会检查本地Bundle与更新服务器上的差异，并下载差异。

程序运行过程中，可由下载器`Downloader`动态下载Bundle到本地。

## 打包工具 Packer
Packer包括三部分的功能：

- 打包配置工具
- 生成Bundle
- 一键打包

### 打包配置工具
打包配置工具主要是设置打包参数与定义Bundle。

#### 打包参数说明

| 配置 | 说明 |
| - | :- |
| App ID | AppID |
| Version | 版本，格式 xxx.yyy.zzz |
| Deveploment | 是否Deveploment开打模式 |
| Enable Update | 是否开启更新 |
| Updat Url | 更新地址 |

#### Bundle定义说明

| 配置 | 说明
| - | :-
| Name | Bundle名，后续会生成`Name.u`的Bundle文件，如`lua.u` |
| Path | Bundle对应资源路径 |
| Pack Mode | Bundle生成模式 |

只有打勾的Bundle定义才会打到包中，未打到包中的Bundle可由下载器`Downloader`后继动态下载。

| Pack Mode | 说明 |
| - | :- |
| All In One | 目录生成单一Bundle |
| Sub Folder | 为目录下每个目录生成Bundle |
| Sub File | 为目录下每个文件生成Bundle |

## 配置工具截图
![Packer](https://gitee.com/xdray/xframe/raw/master/Doc/Res/packer.png)

## 打包工具截图
![Packer](https://gitee.com/xdray/xframe/raw/master/Doc/Res/builder.png)